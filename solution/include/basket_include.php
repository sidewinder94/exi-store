<?php

if(!isset($_SESSION["basket"]))
{
    $_SESSION["basket"] = new Basket();
}

?>

<script type="text/javascript">
	function add_article(id)
    {
    	var nbr = document.getElementById("article_qty_"+id).value;
        window.location.replace("actions/basket.php?add="+id+"&n="+nbr);
    }
    
    function del_article(id)
    {
        window.location.replace("actions/basket.php?del="+id);
    }
    
    function del_all_article()
    {
    	var answer = confirm("Voulez-vous vraiment annuler tous vos achats ?");
		
		if(answer)
		{
			window.location.replace("actions/basket.php?delall=1");
		}
    }
    
    function order()
    {
    	window.location = "basket_view.php?p=1";
    }
</script>

<td rowspan="3" id="basket">
	<?php
		$_SESSION["basket"]->display();
	?>
</td>
