<?php
	include_once("classes/article.class.php");
	include_once("classes/basket.class.php");
    include_once("classes/user.class.php");
	session_start();
	
	if (!isset($_SESSION['connected']))
	{
	    header("Location: ./register_view.php?p=-1");
	}
	else
	{
	    if ($_SESSION['connected'] != True)
	    {
	        header("Location: ./register_view.php?p=-1");
	    }
	}
?>
<!doctype html>
<html>

	<head>
		<title>eXi@store - Musique, film, DVD, Jeux vidéo et bien plus...</title>
		<meta charset="utf-8" />
		<link rel="stylesheet" type="text/css" href="style.css" />
		<link rel="icon" type="image/png" media="screen" href="favicon.png" />
		<link rel="shortcut icon" type="image/x-ico" media="screen" href="favicon.ico" />
	</head>
	
	<body>
		
		<div id="global">
		
		<header>
			<?php include("./include/header_include.php"); ?>
		</header>
		
		<div id="content">
			<table>
			    <tr>
				    <?php include("./include/account_include.php");?>
				    <?php include("./include/basket_include.php");?>
				</tr>
				
				<tr>
				    <?php include("./include/history_include.php");?>
                </tr>
			</table>
		</div>
		
		<footer>
			<?php include("./include/footer_include.php"); ?>
		</footer>
		
		</div>
	<script type="text/javascript" src="js/update_account.js"></script>	
	</body>

</html>
