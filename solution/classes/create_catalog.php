<?php

    /* create_catalog.php
    
        This file get the article data from the database
        and creates a new catalog.
    
    */

    /* instance a catalog in memory */
    $catalog = new Catalog();

	/* get the articles in the database */
	$link = new PDO("mysql:host=exiassistance-sn.fr;dbname=EXAR_BMZ", "EXAR_BMZ", "UPLVVu8JdNPXmANf");
	$query = $link->prepare("SELECT * FROM Produit");
	$query->execute();

    /* for every article */
	while($row = $query->fetch(PDO::FETCH_OBJ))
	{
	    /* create an article in memory */
		$article = new Article($row->ID_produit,
		                       $row->Nom_produit,
		                       $row->Description_produit,
		                       $row->Prix_produit,
		                       $row->Etat_de_production_produit,
		                       $row->Quantite_produit,
		                       $row->ID_categorie);

        /* add it to the catalog */
		$catalog->add($article);
	}

    /* get categories */
    $query = $link->prepare("SELECT * FROM Categorie");
	$query->execute();

	while($row = $query->fetch(PDO::FETCH_OBJ))
	{
	    $catalog->add_category($row->ID_categorie, $row->Nom_categorie);
	}

    /* derefer the connexion */
	$link = null;

?>
