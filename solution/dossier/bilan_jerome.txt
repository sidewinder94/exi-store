Bilan projet PHP Jérôme
--------------------------------------------------------------------------------

Habitué du CSS, ma fonction d'ergonome et ma responsabilité de l'aspect visuel
m'a permis de bien m'amuser tout en fournissant un travail efficace me comblant
de fierté.

Notre groupe était performant et la communication se faisait plutôt bien.

J'ai cependant quelques regrets concernant la partie technique, où une meilleure
utilisation de la POO aurait put être possible. Nous l'avons remarqué un peu tard.

Dans l'ensemble, ce projet conclue agréablement ma première année à l'eXia.
