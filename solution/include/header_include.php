<?php

	function display_login_buttons()
	{
		?>
		<table id="login_table" class="centertable">
			<tr><td><input class="login_button" type="button" value="Se connecter" onClick="button_connect();"/></td></tr>
			<tr><td><input class="login_button" type="button" value="S'inscrire" onClick="button_register();" /></td></tr>
		</table>
		
		<table id="login_form" class="centertable hidden">
			<form action="actions/connect.php?m=login" method="post">
			<tr><td><input name="login" type="text" placeholder="Nom d'utilisateur" /></td></tr>
			<tr><td><input name="password" type="password" placeholder="Mot de passe" /></td></tr>
			<tr><td><input class="login_button" type="submit" value="Go !" /></td></tr>
			</form>
		</table>
		<?php
	}
	
	function display_admin_button($value)
	{
	    if(isset($_SESSION["user"]))
	    {
	        if($_SESSION["user"]->is_admin())
	        {
	            if ($value)
	            {
	                echo "<a class='selected' href='#'>Admin</a>";
	            }
	            else
	            {
	                echo "<a href='./admin_view.php?p=3'>Admin</a>";
	            }
	        }
	    }
	}
	
	function display_username()
	{
		?>
		
		<table class="centertable">
			<tr><td><strong><?php echo $_SESSION['user']->get_login(); ?></strong></td></tr>
			<tr><td><input class="login_button" type="button" value="Déconnexion" onClick="button_disconnect();" /></td></tr>
		</table>
		
		<?php
	}
?>
<script type="text/javascript">
	function button_connect()
	{
		var login_table = document.getElementById("login_table");
		var login_form = document.getElementById("login_form");
		
		login_table.className = "centertable hidden";
		login_form.className = "centertable";
	}
	
	function button_register()
	{
		window.location = "register_view.php?p=-1";
	}
	
	function button_disconnect()
	{
		window.location = "actions/connect.php?m=logout";
	}
</script>

<table>
	<tr>
		<td id="banner"><!-- --></td>
		<td id="login">
			<?php
				if(!isset($_SESSION['connected']))
				{
					display_login_buttons();
				}
				else if (!$_SESSION['connected'])
				{
					display_login_buttons();
				}
				else
				{
					display_username();
				}
			?>
		</td>
	</tr>
	<tr>
		<td id="menu" colspan="2">
		<?php 
			if ((isset($_GET['p'])))
			{
				$page = (int) $_GET['p'];	
			}
			else
			{
				$page =	0;
			}
		?>
		<?php switch($page)
		{
			case 0:?>
			<a class="selected" href="#">Catalogue</a>
			<a href="./basket_view.php?p=1">Panier</a>
			<a href="./account_view.php?p=2">Mon compte</a>
			<?php display_admin_button(False); ?>
		<?php break;
			case 1:?>
			<a href="./index.php?p=0">Catalogue</a>
			<a class="selected" href="#">Panier</a>
			<a href="./account_view.php?p=2">Mon compte</a>
			<?php display_admin_button(False); ?>
		<?php break;
			case 2:?>
			<a href="./index.php?p=0">Catalogue</a>
			<a href="./basket_view.php?p=1">Panier</a>
			<a class="selected" href="#">Mon compte</a>
			<?php display_admin_button(False); ?>
		<?php break;
			case 3:?>
			<a href="./index.php?p=0">Catalogue</a>
			<a href="./basket_view.php?p=1">Panier</a>
			<a href="./account_view.php?p=2">Mon compte</a>
			<?php display_admin_button(True); ?>
		<?php break;
			default:?>
			<a href="./index.php?p=0">Catalogue</a>
			<a href="./basket_view.php?p=1">Panier</a>
			<a href="./account_view.php?p=2">Mon compte</a>
			<?php display_admin_button(False); ?>
		<?php break;}?>

		</td>
	</tr>
</table>
