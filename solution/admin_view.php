<?php
	include_once("classes/article.class.php");
	include_once("classes/basket.class.php");
	include_once("classes/catalog.class.php");
    include_once("classes/user.class.php");
	
	session_start();
	
	/* test if user is admin */
	
	if(isset($_SESSION["user"]))
	{
		if(!$_SESSION["user"]->is_admin())
		{
			header("Location: index.php");
		}
	}
	else
	{
		header("Location: index.php");
	}
	
?>
<!doctype html>
<html>

	<head>
		<title>eXi@store - Musique, film, DVD, Jeux vidéo et bien plus...</title>
		<meta charset="utf-8" />
		<link rel="stylesheet" type="text/css" href="style.css" />
		<link rel="icon" type="image/png" media="screen" href="favicon.png" />
		<link rel="shortcut icon" type="image/x-ico" media="screen" href="favicon.ico" />
	</head>
	
	<body>
		
		<div id="global">
		
		<header>
			<?php include("./include/header_include.php"); ?>
		</header>
		
		<div id="content">
			<table>
			    <tr>
				    <?php include("./include/admin_include.php");?>
				</tr>
			</table>
		</div>
		
		<footer>
			<?php include("./include/footer_include.php"); ?>
		</footer>
		
		</div>
		
	</body>

</html>
