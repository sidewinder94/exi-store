<?php
	include_once("classes/article.class.php");
	include_once("classes/basket.class.php");
	include_once("classes/catalog.class.php");
	include_once("classes/user.class.php");
	
	$catalog = new Catalog();

	/* get the articles in the database */
	$link = new PDO("mysql:host=exiassistance-sn.fr;dbname=EXAR_BMZ", "EXAR_BMZ", "UPLVVu8JdNPXmANf");
	
	if(isset($_GET['id']))
	{
	    $id = $_GET['id'];
	    $query = $link->prepare("SELECT * FROM Produit WHERE ID_produit = :id");
	    $query->execute(array('id' => $id));
	}
	else if(isset($_GET['s']))
	{
	    $search = $_GET['s'];
	    
	    $query_str = "SELECT * FROM Produit WHERE (Nom_produit LIKE :s OR Description_produit LIKE :s)";
	    
	    $category = Null;
	    
	    if(isset($_GET['c']))
	    {
	        $category = (int) $_GET['c'];
	        if($category > 0)
            {
                $query_str .= "AND ID_categorie=:c";
            }
	    }
	    
	    $query = $link->prepare($query_str);
	    $query->execute(array('s' => "%" . $search . "%",
	                          'c' => $category));
	}
	else
	{
	    header("Location: index.php");
	}

	while($row = $query->fetch(PDO::FETCH_OBJ))
	{
		$article = new Article($row->ID_produit, $row->Nom_produit, $row->Description_produit, $row->Prix_produit, $row->Etat_de_production_produit, $row->Quantite_produit, $row->ID_categorie);
		$catalog->add($article);
	}
	
	/* get categories */
    $query = $link->prepare("SELECT * FROM Categorie");
	$query->execute();

	while($row = $query->fetch(PDO::FETCH_OBJ))
	{
	    $catalog->add_category($row->ID_categorie, $row->Nom_categorie);
	}

	$link = null;

	session_start();
?>
<!DOCTYPE html>
<html>

	<head>
		<title>eXi@store - Musique, film, DVD, Jeux vidéo et bien plus...</title>
		<meta charset="utf-8" />
		<link rel="stylesheet" type="text/css" href="style.css" />
		<link rel="icon" type="image/png" media="screen" href="favicon.png" />
		<link rel="shortcut icon" type="image/x-ico" media="screen" href="favicon.ico" />
	</head>
	
	<body>
		
		<div id="global">
		
		<header>
			<?php include("./include/header_include.php"); ?>
		</header>
		
		<div id="content">
			<table>
			    <tr>
					<?php include("./include/search_include.php"); ?>
					<?php include("./include/basket_include.php");?>
				</tr>
			
				<tr>
					<?php include("./include/article_include.php"); ?>
				</tr>

			</table>
		</div>
		
		<footer>
			<?php include("./include/footer_include.php"); ?>
		</footer>
		
		</div>
		
	</body>

</html>
