# script cr�� le : Tue Jun 19 08:59:47 CEST 2012 -   syntaxe MySQL ;

# use  VOTRE_BASE_DE_DONNEE ;

DROP TABLE IF EXISTS Utilisateur ;
CREATE TABLE Utilisateur (ID_utilisateur int AUTO_INCREMENT NOT NULL,
Login_utilisateur CHAR(255),
MDP_utilisateur CHAR(255),
Nom_utilisateur CHAR(255),
Prenom_utilisateur CHAR(255),
Adresse_utilisateur CHAR(255),
Telephone_utilisateur CHAR(255),
Mail_utilisateur CHAR(255),
Admin_utilisateur BOOL,
PRIMARY KEY (ID_utilisateur) ) ENGINE=InnoDB;

DROP TABLE IF EXISTS Produit ;
CREATE TABLE Produit (ID_produit int AUTO_INCREMENT NOT NULL,
Nom_produit CHAR(255),
Description_produit CHAR(255),
Quantite_produit CHAR(255),
Prix_produit INT,
Etat_de_production_produit INT,
ID_TVA INT NOT NULL,
PRIMARY KEY (ID_produit) ) ENGINE=InnoDB;

DROP TABLE IF EXISTS Commande ;
CREATE TABLE Commande (ID_commande int AUTO_INCREMENT NOT NULL,
Date_commande CHAR(255),
Adresse_livraison_commande CHAR(255),
Montant_total_commande CHAR(255),
ID_utilisateur INT NOT NULL,
ID_carte INT NOT NULL,
PRIMARY KEY (ID_commande) ) ENGINE=InnoDB;

DROP TABLE IF EXISTS TVA ;
CREATE TABLE TVA (ID_TVA int AUTO_INCREMENT NOT NULL,
Valeur_TVA CHAR(255),
PRIMARY KEY (ID_TVA) ) ENGINE=InnoDB;

DROP TABLE IF EXISTS Type_carte ;
CREATE TABLE Type_carte (ID_carte int AUTO_INCREMENT NOT NULL,
Nom_carte CHAR(255),
PRIMARY KEY (ID_carte) ) ENGINE=InnoDB;

DROP TABLE IF EXISTS LIGNE_PRODUIT ;
CREATE TABLE LIGNE_PRODUIT (ID_commande int AUTO_INCREMENT NOT NULL,
ID_produit INT NOT NULL,
Quantite CHAR(255),
PRIMARY KEY (ID_commande,
 ID_produit) ) ENGINE=InnoDB;

ALTER TABLE Produit ADD CONSTRAINT FK_Produit_ID_TVA FOREIGN KEY (ID_TVA) REFERENCES TVA (ID_TVA);

ALTER TABLE Commande ADD CONSTRAINT FK_Commande_ID_utilisateur FOREIGN KEY (ID_utilisateur) REFERENCES Utilisateur (ID_utilisateur);
ALTER TABLE Commande ADD CONSTRAINT FK_Commande_ID_carte FOREIGN KEY (ID_carte) REFERENCES Type_carte (ID_carte);
ALTER TABLE LIGNE_PRODUIT ADD CONSTRAINT FK_LIGNE_PRODUIT_ID_commande FOREIGN KEY (ID_commande) REFERENCES Commande (ID_commande);
ALTER TABLE LIGNE_PRODUIT ADD CONSTRAINT FK_LIGNE_PRODUIT_ID_produit FOREIGN KEY (ID_produit) REFERENCES Produit (ID_produit);
