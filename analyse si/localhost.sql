-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Mar 19 Juin 2012 à 09:42
-- Version du serveur: 5.1.61-0+squeeze1
-- Version de PHP: 5.3.13-1~dotdeb.0

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `EXAR_BMZ`
--
CREATE DATABASE `EXAR_BMZ` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `EXAR_BMZ`;

-- --------------------------------------------------------

--
-- Structure de la table `Commande`
--

CREATE TABLE IF NOT EXISTS `Commande` (
  `ID_commande` int(11) NOT NULL AUTO_INCREMENT,
  `Date_commande` char(255) DEFAULT NULL,
  `Adresse_livraison_commande` char(255) DEFAULT NULL,
  `Montant_total_commande` char(255) DEFAULT NULL,
  `ID_utilisateur` int(11) NOT NULL,
  `ID_carte` int(11) NOT NULL,
  PRIMARY KEY (`ID_commande`),
  KEY `FK_Commande_ID_utilisateur` (`ID_utilisateur`),
  KEY `FK_Commande_ID_carte` (`ID_carte`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `LIGNE_PRODUIT`
--

CREATE TABLE IF NOT EXISTS `LIGNE_PRODUIT` (
  `ID_commande` int(11) NOT NULL AUTO_INCREMENT,
  `ID_produit` int(11) NOT NULL,
  `Quantite` char(255) DEFAULT NULL,
  PRIMARY KEY (`ID_commande`,`ID_produit`),
  KEY `FK_LIGNE_PRODUIT_ID_produit` (`ID_produit`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `Produit`
--

CREATE TABLE IF NOT EXISTS `Produit` (
  `ID_produit` int(11) NOT NULL AUTO_INCREMENT,
  `Ref_produit` varchar(255) NOT NULL,
  `Nom_produit` char(255) DEFAULT NULL,
  `Description_produit` char(255) DEFAULT NULL,
  `Quantite_produit` char(255) DEFAULT NULL,
  `Prix_produit` float NOT NULL DEFAULT '0',
  `Etat_de_production_produit` varchar(255) DEFAULT NULL,
  `ID_TVA` int(11) NOT NULL,
  PRIMARY KEY (`ID_produit`),
  KEY `FK_Produit_ID_TVA` (`ID_TVA`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `TVA`
--

CREATE TABLE IF NOT EXISTS `TVA` (
  `ID_TVA` int(11) NOT NULL AUTO_INCREMENT,
  `Valeur_TVA` char(255) DEFAULT NULL,
  PRIMARY KEY (`ID_TVA`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `Type_carte`
--

CREATE TABLE IF NOT EXISTS `Type_carte` (
  `ID_carte` int(11) NOT NULL AUTO_INCREMENT,
  `Nom_carte` char(255) DEFAULT NULL,
  PRIMARY KEY (`ID_carte`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `Utilisateur`
--

CREATE TABLE IF NOT EXISTS `Utilisateur` (
  `ID_utilisateur` int(11) NOT NULL AUTO_INCREMENT,
  `Login_utilisateur` char(255) DEFAULT NULL,
  `MDP_utilisateur` char(255) DEFAULT NULL,
  `Nom_utilisateur` char(255) DEFAULT NULL,
  `Prenom_utilisateur` char(255) DEFAULT NULL,
  `Adresse_utilisateur` char(255) DEFAULT NULL,
  `Telephone_utilisateur` char(255) DEFAULT NULL,
  `Mail_utilisateur` char(255) DEFAULT NULL,
  `Admin_utilisateur` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`ID_utilisateur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `Commande`
--
ALTER TABLE `Commande`
  ADD CONSTRAINT `FK_Commande_ID_carte` FOREIGN KEY (`ID_carte`) REFERENCES `Type_carte` (`ID_carte`),
  ADD CONSTRAINT `FK_Commande_ID_utilisateur` FOREIGN KEY (`ID_utilisateur`) REFERENCES `Utilisateur` (`ID_utilisateur`);

--
-- Contraintes pour la table `LIGNE_PRODUIT`
--
ALTER TABLE `LIGNE_PRODUIT`
  ADD CONSTRAINT `FK_LIGNE_PRODUIT_ID_produit` FOREIGN KEY (`ID_produit`) REFERENCES `Produit` (`ID_produit`),
  ADD CONSTRAINT `FK_LIGNE_PRODUIT_ID_commande` FOREIGN KEY (`ID_commande`) REFERENCES `Commande` (`ID_commande`);

--
-- Contraintes pour la table `Produit`
--
ALTER TABLE `Produit`
  ADD CONSTRAINT `FK_Produit_ID_TVA` FOREIGN KEY (`ID_TVA`) REFERENCES `TVA` (`ID_TVA`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
