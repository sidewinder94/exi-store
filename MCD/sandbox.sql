CREATE TABLE `Produit` (
  `ID_Produit int(11)`,
  `Ref_produit char(255)`,
  `Nom_produit char(255)`,
  `Description_Produit char(255)`,
  `Quantité_produit char(255)`,
  `Prix_produit char(255)`,
  `Etat_de_production_produit int(1)`,
  `ID_Catégorie int(11)`,
  PRIMARY KEY(`ID_Produit int(11)`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE `Utilisateur` (
  `ID_utilisateur int(11)`,
  `Login_utilisateur char(255)`,
  `MDP_utilisateur char(255)`,
  `Nom_utilisateur char(255)`,
  `Prenom_utilisateur char(255)`,
  `Adresse_utilisateur char(255)`,
  `Code_postal_utilisateur int(6)`,
  `Ville_utilisateur char(255)`,
  `Telephone_utilisateur char(255)`,
  `Mail_utilisateur char(255)`,
  `Admin_utilisateur tinnyint(1)`,
  PRIMARY KEY(`ID_utilisateur int(11)`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE `CONTENIR` (
  `ID_Commande int(11)`,
  `ID_Produit int(11)`,
  `Quantité char(255)`,
  PRIMARY KEY(`ID_Commande int(11)`, `ID_Produit int(11)`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE `Catégorie` (
  `ID_Catégorie int(11)`,
  `Nom_catégorie char(255)`,
  PRIMARY KEY(`ID_Catégorie int(11)`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE `Commande` (
  `ID_Commande int(11)`,
  `Date_Commande char(255)`,
  `Adresse_livraison_commande char(255)`,
  `Montant_total_commande char(255)`,
  `ID_Carte int(11)`,
  `ID_utilisateur int(11)`,
  PRIMARY KEY(`ID_Commande int(11)`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE `Type Carte` (
  `ID_Carte int(11)`,
  `Nom_Carte char(255)`,
  PRIMARY KEY(`ID_Carte int(11)`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE `TVA` (
  `ID_TVA int(11)`,
  `Valeur_TVA char(255)`,
  `ID_Produit int(11)`,
  PRIMARY KEY(`ID_TVA int(11)`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

ALTER TABLE `Produit` ADD FOREIGN KEY (`ID_Catégorie int(11)`) REFERENCES `Catégorie` (`ID_Catégorie int(11)`);

ALTER TABLE `CONTENIR` ADD FOREIGN KEY (`ID_Commande int(11)`) REFERENCES `Commande` (`ID_Commande int(11)`);

ALTER TABLE `CONTENIR` ADD FOREIGN KEY (`ID_Produit int(11)`) REFERENCES `Produit` (`ID_Produit int(11)`);

ALTER TABLE `Commande` ADD FOREIGN KEY (`ID_Carte int(11)`) REFERENCES `Type Carte` (`ID_Carte int(11)`);

ALTER TABLE `Commande` ADD FOREIGN KEY (`ID_utilisateur int(11)`) REFERENCES `Utilisateur` (`ID_utilisateur int(11)`);

ALTER TABLE `TVA` ADD FOREIGN KEY (`ID_Produit int(11)`) REFERENCES `Produit` (`ID_Produit int(11)`);