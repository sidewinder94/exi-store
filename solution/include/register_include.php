<form method="post" action="./actions/connect.php?m=register" id="form_register">
	<fieldset>
	<legend>Inscription</legend>
		<table id='register'>
			<tr>
				<td>
					<fieldset>
						<legend>Informations de connexion</legend>
						<table>
							<tr>
								<td id="form_login">Nom d'utilisateur</td>
								<td colspan="3">
								<input type="text" name="text_login"/>
								<a href="#" class="help_link"><img src="images/help.png" /></a>
								<span class="help">Votre nom d'utilisateur peut contenir des majuscules, des minuscules, et des tirets. Il ne doit pas contenir d'espace.</span>
								</td>
							</tr>
							<tr>
								<td id="form_password">Mot de passe</td>
								<td colspan="3">
								<input type="password" name="text_password" />
								<a href="#" class="help_link"><img src="images/help.png" /></a>
								<span class="help">Votre mot de passe peut contenir des majuscules, des minuscules, des accents et des tirets. Il ne doit pas contenir d'espace.</span>
								</td>
							</tr>
							<tr>
								<td id="form_confirm_password">Confirmation mot de passe</td>
								<td colspan="3">
								<input type="password" name="text_confirm_password" />
								</td>
							<tr>
								<td id="form_email">Adresse email</td>
								<td colspan="3">
								<input type="text" name="text_email" />
								<a href="#" class="help_link"><img src="images/help.png" /></a>
								<span class="help">Votre adresse email doit être tout en minuscule, sans espaces. Les tirets sont autorisés.</span>
								</td>
							</tr>
							<tr>
								<td id="form_confirm_email">Confirmation email</td>
								<td colspan="3">
								<input type="text" name="text_confirm_email" />
								</td>
							</tr>
							</tr>
						</table>
					</fieldset>
				</td>
				<td>
					<fieldset>
						<legend>Informations personnelles</legend>
						<table>
							<tr>
								<td id="form_name">Prénom</td>
								<td colspan="3">
								<input type="text" name="text_name" />
								<a href="#" class="help_link"><img src="images/help.png" /></a>
								<span class="help">Votre prénom peut contenir des majuscules, des minuscules, des accents et des tirets. Il ne doit pas contenir d'espace.</span>
								</td>
							</tr>
							 <tr>
								<td id="form_surname">Nom</td>
								<td colspan="3">
								<input type="text" name="text_surname" />
								<a href="#" class="help_link"><img src="images/help.png" /></a>
								<span class="help">Votre nom peut contenir des majuscules, des minuscules, des accents et des tirets. Il ne doit pas contenir d'espace.</span>
								</td>
							</tr>
							<tr>
								<td id="form_adress">Adresse</td>
								<td colspan="3">
								<input type="text" name="text_adress" />
								</td>
							</tr>
							<tr>
								<td id="form_cp">Code postal</td>
								<td colspan="3">
								<input type="text" name="text_cp" />
								</td>
							</tr>
							<tr>
								<td id="form_city">Ville</td>
								<td colspan="3">
								<input type="text" name="text_city" />
								</td>
							</tr>
							<tr>
								<td id="form_phone">Téléphone</td>
								<td colspan="3">
								<input type="text" name="text_phone" />
								</td>
							</tr>
						</table>
					</fieldset>
				</td>
			</tr>
			<tr>
				<th class="centered" colspan="4">
				<input type="button" name="submit_button" value="Envoyer" onClick="subscribe(this.form);" />
				</th>
			</tr>
		</table>
	</fieldset>
</form>
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
