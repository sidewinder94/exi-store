<?php
	include_once("../classes/article.class.php");
	include_once("../classes/catalog.class.php");
	include_once("../classes/basket.class.php");

	session_start();

	/* connection to the database */
	$link = new PDO("mysql:host=exiassistance-sn.fr;dbname=EXAR_BMZ", "EXAR_BMZ", "UPLVVu8JdNPXmANf");
	$query = $link->prepare("INSERT INTO Produit (Nom_produit, Description_produit, Quantite_produit, Prix_produit, Etat_de_production_produit, ID_categorie) VALUES (:name, :description, :stock, :price, :state, :category)");
	
	/* récupération des données */
	if($_POST['name'] != "" && $_POST['description'] != "" && $_POST['stock'] != "" && $_POST['price'] != "" && $_POST['state'] != "" && $_POST['category'] != "")
	{
		$name = $_POST['name'];
		$description = $_POST['description'];
		$stock = $_POST['stock'];
		$price = $_POST['price'];
		$state = $_POST['state'];
		$category = $_POST['category'];
	}
	else
	{
		header("Location: ../admin_view.php?p=-1");
	}
	
	$query->execute(array('name'=>$name, 'description'=>$description, 'stock'=>$stock, 'price'=>$price, 'state'=>$state, 'category'=>$category));

	$link = null;
	
	header("Location: ../admin_view.php?p=-1");
?>