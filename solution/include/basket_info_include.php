<?php

if(!isset($_SESSION["basket"]))
{
    $_SESSION["basket"] = new Basket();
}

?>

<script type="text/javascript">
	function add_article(id)
    {
    	var nbr = document.getElementById("article_qty_"+id).value;
        window.location.replace("actions/basket.php?add="+id+"&n="+nbr);
    }
    
    function del_article(id)
    {
        window.location.replace("actions/basket.php?del="+id);
    }
    
    function del_all_article()
    {
    	var answer = confirm("Voulez-vous vraiment annuler tous vos achats ?");
		
		if(answer)
		{
			window.location.replace("actions/basket.php?delall=1");
		}
    }
    
    function order()
    {
        var card = document.getElementById("combobox_card").selectedIndex + 1;
        /*TODO:Changer les alerts et ajouter un test pour la date d'expiration*/
        if (document.getElementById("credit_card_number").value.length == 10)
        {
            if(document.getElementById("security_card_code").value.length == 3)
            {
                    window.location = "actions/order.php?c="+card;
            }
            else
            {
                alert("Code de sécurité invalide");
            }    
        }
        else
        {
            alert("Numéro de carte invalide");
        }
    }
</script>

<td rowspan="3" id="basket_like_catalog">
	<?php
		$_SESSION["basket"]->display_like_catalog();
	?>
</td>
