<script type="text/javascript">
    function search_article()
    {
        var s = document.getElementById("searchbar_text").value;
        
        var select = document.getElementById("searchbar_category");
        var c = select.options[select.selectedIndex].value;
        
        /* easter egg */
        if(s == "La grande question sur la vie, l'univers et le reste")
        {
            window.location = "http://fr.wikipedia.org/wiki/La_grande_question_sur_la_vie,_l%27univers_et_le_reste";
        }
        else
        {
            window.location = "article_view.php?p=-1&s=" + s;
            
            if(c > 0)
            {
                window.location = "article_view.php?p=-1&s=" + s + "&c=" + c;
            }
        }
    }
</script>
<td id="searchbar">
	<input id="searchbar_text" type="text" placeholder="Rechercher..." />
	<?php echo $catalog->display_categories(-1); ?>
	<input id="searchbar_button" type="button" value="Go !" onClick="search_article();" >
</td>

