function subscribe(form)
{
    /* error messages */
    var abort = false;
	var error_login = document.getElementById("form_login");
	var error_password = document.getElementById("form_password");
	var error_confirm_password = document.getElementById("form_confirm_password");
	var error_adress = document.getElementById("form_adress");
	var error_cp = document.getElementById("form_cp");
	var error_city = document.getElementById("form_city");
	var error_phone = document.getElementById("form_phone");
    var error_surname = document.getElementById("form_surname");
    var error_name = document.getElementById("form_name");
    var error_email = document.getElementById("form_email");
    var error_confirm_email = document.getElementById("form_confirm_email");
    
    /* get the surname */
    var surname = form.text_surname.value;
    if(!verify_name(surname))
    {
        error_surname.className = "error";
        abort = true;
    }
    else
    {
        error_surname.className = "";
    }
    
    /* get the name */
    var name = form.text_name.value;
    if(!verify_name(name))
    {
        error_name.className = "error";
        abort = true;
    }
    else
    {
        error_name.className = "";
    }
    
	/* get the login */
	
	var login = form.text_login.value;
	if(!verify_login(login))
	{
		error_login.className = "error";
		abort = true;
	}
	else
	{
		error_login.className = "";
	}
	
	/* check password */
	var password = form.text_password.value;
	var confirm_password = form.text_confirm_password.value;
	if(!verify_password(password, confirm_password))
	{
		error_password.className = "error";
		error_confirm_password.className = "error";
		abort = true;
	}
	else
	{
		error_password.className = "";
		error_confirm_password.className = "";
	}
	
    /* check email */
    var email = form.text_email.value;
    var confirm_email = form.text_confirm_email.value;
    if(!verify_email(email, confirm_email))
    {
        error_email.className = "error";
        error_confirm_email.className = "error";
        abort = true;
    }
    else
    {
        error_email.className = "";
        error_confirm_email.className = "";
    }
	
	/* check adress */
	var adress = form.text_adress.value;
	var cp = form.text_cp.value;
	var city = form.text_city.value;
	var phone = form.text_phone.value;
	if(!adress || !cp || !phone || !phone)
	{
		error_adress.className = "error";
		error_cp.className = "error";
		error_city.className = "error";
		error_phone.className = "error";
		abort = true;
	}
    
    /* quit if errors */
    if(abort)
    {
        return false;
    }
    else
    {
        /* inscription */
		form.submit();
    }
    
}

function verify_email(email, confirm)
{
    if(!email || !confirm || email != confirm)
    {
        return false;
    }
    else
    {
        /* regex mail */
        var regex = new RegExp("^[0-9a-z._-]+@[0-9a-z-]+[.]{1}[a-z]{2,3}$");
        
        if(regex.test(email) && regex.test(confirm))
        {
            return true;
        }
    }
}

function verify_password(password, confirm_password)
{
	if(!password || !confirm_password || password != confirm_password)
	{
		return false;
	}
	else
	{
		/* regex password */
        var regex = new RegExp("^[0-9a-zA-ZÁÀÂÄÉÈÊËÍÌÎÏÓÒÔÖÚÙÛÜáàâäéèêëíìîïóòôöúùûüÇçŒœ_-]+$");
        
        if(regex.test(password) && regex.test(confirm_password))
        {
            return true;
        }
	}
}

function verify_name(name)
{
    if(!name)
    {
        return false;
    }
    else
    {
        /* regex */
        var regex = new RegExp("^[a-zA-ZÁÀÂÄÉÈÊËÍÌÎÏÓÒÔÖÚÙÛÜáàâäéèêëíìîïóòôöúùûüÇçŒœ-]+$");
        
        if(regex.test(name))
        {
            return true;
        }
    }
}

function verify_login(login)
{
	if(!login)
	{
		return false;
	}
	else
	{
		/* regex */
		var regex = new RegExp("^[a-zA-Z_-]+$");
		
		if(regex.test(login))
		{
			return true;
		}
	}	
}
