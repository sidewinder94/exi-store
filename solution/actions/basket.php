<?php

include_once("../classes/article.class.php");
include_once("../classes/catalog.class.php");
include_once("../classes/basket.class.php");

session_start();

$catalog = new Catalog();

/* get the articles in the database */
$link = new PDO("mysql:host=exiassistance-sn.fr;dbname=EXAR_BMZ", "EXAR_BMZ", "UPLVVu8JdNPXmANf");
$query = $link->prepare("SELECT * FROM Produit");
$query->execute();

while($row = $query->fetch(PDO::FETCH_OBJ))
{
	$article = new Article($row->ID_produit, $row->Nom_produit, $row->Description_produit, $row->Prix_produit, $row->Etat_de_production_produit, $row->Quantite_produit, $row->ID_categorie);
	$catalog->add($article);
}

$link = null;

/* add an element to the basket */
if(isset($_GET["add"]))
{
    $id = (int) $_GET["add"];
    
    $nbr = 1;
    
    if(isset($_GET["n"]))
    {
    	$nbr = (int) $_GET["n"];
    }
    
    if($nbr >= 1)
	{
		$_SESSION["basket"]->add($catalog->get_article($id), $nbr);
	}
}

/* delete an element from the basket */
if(isset($_GET["del"]))
{
    $id = (int) $_GET["del"];
    
    $_SESSION["basket"]->del($id);
}

/* delete all the elements from the basket */
if(isset($_GET["delall"]))
{
    $val = (int) $_GET["delall"];
    
    if($val)
    {
        $_SESSION["basket"]->del_all();
    }
}

/* return to previous page */
header("Location: ../index.php");

?>
