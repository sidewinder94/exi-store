<?php
    error_reporting(E_STRICT);
    include_once("../classes/article.class.php");
    include_once("../classes/basket.class.php");
    include_once("../classes/user.class.php");
    include_once("../classes/phpmailer.class.php");
    include_once("../classes/class.smtp.php");
    
    session_start();
    
    function smtpMailer($to,$from,$from_name,$subject,$body) 
    {
        $mail = new PHPMailer();
        $mail->IsSMTP(); // telling the class to use SMTP
        $mail->Host       = "smtp.gmail.com"; // SMTP server
        $mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
                                                // 1 = errors and messages
                                                // 2 = messages only
        $mail->SMTPAuth   = true;                  // enable SMTP authentication
        $mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
        $mail->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
        $mail->Port       = 465;                   // set the SMTP port for the GMAIL server
        $mail->Username   = "projetweb@exiassistance-sn.fr";  // GMAIL username
        $mail->Password   = "projetweb";            // GMAIL password
        $mail->SetFrom($from, $from_name);
        $mail->AddReplyTo($from, $from_name);
        $mail->Subject    = "Récapitulatif de votre commande";
        $mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
        $mail->MsgHTML($body);
        $mail->AddAddress($to);
        if(!$mail->Send()) 
        {
        echo "Mailer Error: " . $mail->ErrorInfo;
        }  
        else
        {
        echo "Message sent!";
        }
    }
    
    if(isset($_SESSION["connected"]))
    {
        if($_SESSION["connected"] and $_SESSION["user"] and $_SESSION["basket"])
        {
            /* order infos */
            $date = date("d/m/Y");
            $adress = $_SESSION["user"]->get_adress();
            $zip = $_SESSION["user"]->get_zip();
            $city = $_SESSION["user"]->get_city();
            $total = $_SESSION["basket"]->get_total_price();
            $user_id = $_SESSION["user"]->get_id();
            $card_id = (int) $_GET["c"];
            $total_adress = $adress ." ". $zip ." ".$city;
            
	        $link = new PDO("mysql:host=exiassistance-sn.fr;dbname=EXAR_BMZ", "EXAR_BMZ", "UPLVVu8JdNPXmANf");
	        
	        /* insert order */
	        $query = $link->prepare("INSERT INTO Commande VALUES(?, ?, ?, ?, ?, ?)");
	        $query->execute(array(NULL, $date, $total_adress, $total, $user_id, $card_id));
	        
	        /* get the newly created id */
	        $query = $link->prepare("SELECT MAX(ID_commande) FROM Commande");
	        $query->execute();
	        $row = $query->fetch(PDO::FETCH_NUM);
	        $order_id = $row[0];

	        /* insert order lines */
	        $query = $link->prepare("INSERT INTO LIGNE_PRODUIT VALUES(?, ?, ?)");
	        
	        foreach($_SESSION["basket"]->get_articles() as $article)
	        {
	            $query->execute(array($order_id, $article->get_id(), $article->get_qty()));
	        }

	        $link = null;
            
	        /*Envoi d'un mail récapitulatif au client*/
            $mail_body = "Votre commade à bien été prise en compte".
                    ", nous vous remercions d'avoir réalisé vos acahts chez exiastore<br/>".
                    "Vous trouverez ci-dessous un récapitulatif de votre commande: <br/>".
                    "ID de votre commande : ".$order_id."<br/><table>";
             $mail_body .= "<tr>";
             $mail_body .= "<td>Nom Produit</td><td>Description Produit</td><td>Prix unitaire</td><td>Quantité</td><td>Prix total</td>";
             $mail_body .= "</tr>";
            foreach($_SESSION["basket"]->get_articles() as $article)
            {
                $mail_body .= "<tr>";
                $mail_body .= "<td>".$article->get_name().
                        "</td><td>".$article->get_description().
                        "</td><td>".$article->get_price().
                        "</td><td>".$article->get_qty().
                        "</td><td>".$article->get_price()*$article->get_qty()."</td>";
                $mail_body .= "</tr>";
            }
            $mail_body .= "<br/>Total de la commande : ".$total;
            smtpMailer($_SESSION["user"]->get_mail(),"projetweb@exiassistance-sn.fr",
                    "exi@store","Recapitulatif de votre ommande",$mail_body);
	        header("Location: ../account_view.php?p=2");
        }
        else
        {
            header("Location: ../register_view.php");
        }
    }
    else
    {
        header("Location: ../register_view.php");
    }

?>
