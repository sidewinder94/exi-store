<?php

    /*Imports necessary classes*/
    include_once('../classes/user.class.php');
	/* open a session */
	session_start();   
    
	function go_to_url($page)
	{
		header("Location: /" . $page);
	}
		   
	function return_to_index()
	{
		go_to_url("index.php");
	}
		
	function return_to_index_err($message)
	{
		go_to_url("index.php?err=" . $message);
	}
		
	function regex_test($login, $password)
	{
		if(!preg_match("#^[a-zA-Z0-9_-]+#", $login)
		|| !preg_match("#^[a-zA-Z0-9_-]+#", $password))
		{
			return false;
		}
		else
		{
			return true;
		}
	}
		
	function start_session($user)
	{
		$_SESSION['connected'] = true;
		$_SESSION['user'] = new User($user->ID_utilisateur, 
                $user->Login_utilisateur, 
                $user->Nom_utilisateur, 
                $user->Prenom_utilisateur, 
                $user->Adresse_utilisateur, 
                $user->Code_postal_utilisateur, 
                $user->Ville_utilisateur, 
                $user->Telephone_utilisateur, 
                $user->Mail_utilisateur);
        if($user->Admin_utilisateur == 1)
        {
            $_SESSION['user']->set_admin(1);
        }
	}
	
	function close_session()
	{
		$_SESSION['connected'] = false;
		$_SESSION['user'] = Null;
		$_SESSION['basket'] = Null;
	}
	
	
	if(isset($_GET['m']))
	{
		$method = (string) htmlspecialchars($_GET['m']);
		
		if(!in_array($method, array("login", "logout", "register","update")))
		{
			return_to_index();
		}
		else
		{
			/* connect to database */
			$link = new PDO("mysql:host=mwinandy.fr;dbname=EXAR_BMZ", "EXAR_BMZ", "UPLVVu8JdNPXmANf");
			
			/* here starts the deal */
			switch($method)
			{
				case "register":
					/* get post */
					$login = (string) htmlspecialchars($_POST['text_login']);
					$password = sha1((string) htmlspecialchars($_POST['text_password']));
					$confirm_password = sha1((string) htmlspecialchars($_POST['text_confirm_password']));
					$mail = (string) htmlspecialchars($_POST['text_email']);
					$confirm_mail = (string) htmlspecialchars($_POST['text_confirm_email']);
					$nom = (string) htmlspecialchars($_POST['text_surname']);
					$prenom = (string) htmlspecialchars($_POST['text_name']);
					$adresse = (string) htmlspecialchars($_POST['text_adress']);
					$cp = (string) htmlspecialchars($_POST['text_cp']);
					$city =	(string) htmlspecialchars($_POST['text_city']);
					$phone_number = (string) htmlspecialchars($_POST['text_phone']);

					
					/* regex */
					if(!regex_test($login, $password))
					{
						return_to_index_err("Mot de passe ou login invalide pour l'inscription");
						exit;
					}
					
					/*check if the main and confirmation mails and passwords are identical*/
					if ($password != $confirm_password)
					{
						return_to_index_err("Mots de passes différends");
						exit;
					}
					elseif ($mail != $confirm_mail)
					{
						return_to_index_err("Adresses mail différentes");
						exit;
					}
					/*Check if the mail adress already exists*/
                    $query = $link->prepare("SELECT ID_utilisateur FROM Utilisateur WHERE Mail_utilisateur = :mail");
					$query->execute(array("mail" => $mail));
					if($query->fetch())
					{
						return_to_index_err("Vous ne pouvez pas utiliser ce login");
						exit;
					}
					/* check if user already exists*/
					$query = $link->prepare("SELECT ID_utilisateur FROM Utilisateur WHERE Login_utilisateur = :username");
					$query->execute(array("username" => $login));
					
					if($query->fetch())
					{
						return_to_index_err("Vous ne pouvez pas utiliser ce login");
						exit;
					}
					else
					{
						/* insert into the database */
						$query = $link->prepare("INSERT INTO Utilisateur(ID_utilisateur, Login_utilisateur, MDP_utilisateur, Nom_utilisateur, Prenom_utilisateur, Adresse_utilisateur,Code_postal_utilisateur,Ville_utilisateur,Telephone_utilisateur, Mail_utilisateur, Admin_utilisateur) VALUES (?,?,?,?,?,?,?,?,?,?,?)");
						$query->execute(array(NULL, $login, $password,$nom,$prenom,$adresse,$cp,$city,$phone_number,$mail,0)); 
						
						/* start session */
                        $query = $link->prepare("SELECT * FROM Utilisateur WHERE Login_utilisateur = :user_login");
                        $query->execute(array("user_login" => $login));
                        $query_result = $query->fetch(PDO::FETCH_OBJ);
						start_session($query_result);
						
						/* go to confirmation */
						return_to_index();
					}
					break;
				
				case "login":
					/* get post */
					$login = (string) htmlspecialchars($_POST['login']);
					$password = sha1((string) htmlspecialchars($_POST['password']));
					
					/* regex */
					if(!regex_test($login, $password))
					{
						return_to_index_err("Mot de passe ou login invalide");
						exit;
					}
		
					/* check login and password */
					$query = $link->prepare("SELECT ID_utilisateur FROM Utilisateur WHERE Login_utilisateur = :username AND MDP_utilisateur = :password");
					$query->execute(array("username" => $login,
										  "password" => $password));
					
					$rows = $query->fetch(PDO::FETCH_OBJ);
					
					if ($rows)
					{
                        $query = $link->prepare("SELECT * FROM Utilisateur WHERE ID_utilisateur = :user_id");
                        $query->execute(array("user_id"=> $rows->ID_utilisateur));
                        $query_result = $query->fetch(PDO::FETCH_OBJ);
						/* start the session */
						start_session($query_result);
						
						/* go to account */
						return_to_index();
					}
					else
					{
						/* user does not exist or password is incorrect */
						return_to_index_err("Mot de passe ou login invalide");
					}
					break;
				
				case "logout":
					/* disconnect the session */
					close_session();
					
					/* go to index */
					return_to_index();
					break;
				case "update":
	                /*Get the informations*/
	                $login = (string) htmlspecialchars($_POST['text_login']);
	                $password = sha1((string) htmlspecialchars($_POST['text_password']));
	                $confirm_password = sha1((string) htmlspecialchars($_POST['text_confirm_password']));
	                $mail = (string) htmlspecialchars($_POST['text_email']);
	                $confirm_mail = (string) htmlspecialchars($_POST['text_confirm_email']);
	                $nom = (string) htmlspecialchars($_POST['text_surname']);
	                $prenom = (string) htmlspecialchars($_POST['text_name']);
	                $adresse = (string) htmlspecialchars($_POST['text_adress']);
	                $cp = (string) htmlspecialchars($_POST['text_cp']);
	                $city = (string) htmlspecialchars($_POST['text_city']);
	                $phone_number = (string) htmlspecialchars($_POST['text_phone']);
	
	                /*If the user wants to alter his password*/
	                if ($password != "")
	                {
	                    /* regex */
	                    if(!regex_test($login, $password))
	                    {
  	                            return_to_index_err("Mot de passe ou login invalide pour l'inscription");
	                            exit;
	                    }
	
	                    /*check if the main and confirmation mails and passwords are identical*/
	                    if ($password != $confirm_password)
	                    {
	                            return_to_index_err("Mots de passes différends");
	                            exit;
	                    }
	                    elseif ($mail != $confirm_mail)
	                    {
	                            return_to_index_err("Adresses mail différentes");
	                            exit;
	                    }
	                    /*Prepare the query*/
	                    $query = $link->prepare("UPDATE Utilisateur SET MDP_utilisateur=:password,Nom_utilisateur=:user_name,Prenom_utilisateur=:user_first_name,Adresse_utilisateur=:user_adress,Code_postal_utilisateur=:user_cp,Ville_utilisateur=:user_city,Telephone_utilisateur=:user_phone,Mail_utilisateur=:user_mail WHERE Login_utilisateur=:user_login");
	                    $query->execute(array("password" => $password,"user_name" => $nom,"user_first_name" => $prenom,"user_adress" => $adresse,"user_cp"=>$cp,"user_city"=>$city,"user_phone" => $phone_number,"user_mail" => $mail,"user_login" => $login));         
                        
                        /*Update the current user*/
                        $query = $link->prepare("SELECT * FROM Utilisateur WHERE Login_utilisateur = :user_login");
                        $query->execute(array("user_login" => $login));
                        $query_result = $query->fetch(PDO::FETCH_OBJ);
                        start_session($query_result);
	                }
	                /*If there is no password modification*/
	                else
	                {
	                    /* regex */
	                    if(!regex_test($login, "a"))
	                    {
	                            return_to_index_err("Mot de passe ou login invalide pour l'inscription");
	                            exit;
	                    }
	                    /*Compare the two mail adresses*/
	                    if ($mail != $confirm_mail)
	                    {
	                            return_to_index_err("Adresses mail différentes");
	                            exit;
	                    }
	                    /*Prepare the query*/
	                    $query = $link->prepare("UPDATE Utilisateur SET Nom_utilisateur=:user_name,Prenom_utilisateur=:user_first_name,Adresse_utilisateur=:user_adress,Code_postal_utilisateur=:user_cp,Ville_utilisateur=:user_city,Telephone_utilisateur=:user_phone,Mail_utilisateur=:user_mail WHERE Login_utilisateur=:user_login");
	                    $query->execute(array("user_name" => $nom,"user_first_name" => $prenom,"user_adress" => $adresse,"user_cp"=> $cp,"user_city"=>$city,"user_phone" => $phone_number,"user_mail" => $mail,"user_login" => $login));
                        /*Update the current user*/
                        $query = $link->prepare("SELECT * FROM Utilisateur WHERE Login_utilisateur = :user_login");
                        $query->execute(array("user_login" => $login));
                        $query_result = $query->fetch(PDO::FETCH_OBJ);
                        start_session($query_result);
	                }
				default:
					return_to_index();
					break;
			}
		}
	}
	else
	{
		return_to_index();
	}
?>
