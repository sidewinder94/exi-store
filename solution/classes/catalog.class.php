<?php

	class Catalog
    {
        private $articles = array();
        private $categories = array();
        
        public function add($article)
        {
            /* check if article already exists */
            $id = $article->get_id();
            if(!isset($this->articles[$id]))
            {
                $this->articles[$id] = $article;
            }
        }
        
        public function get_article($id)
        {
            if(isset($this->articles[$id]))
            {
                return $this->articles[$id];
            }
            else
            {
                return null;
            }
        }
        
        public function add_category($id, $category)
        {
            $this->categories[$id] = $category;
        }
        
        public function display_categories($active)
        {
            $string = "";
            
            if($active == -1)
            {
                $string .= "<select id='searchbar_category'>";
                $string .= "<option selected='true' value='-1'>Tout</option>";
            }
            else
            {
                $string .= "<select name='category'>";
            }
            
            foreach($this->categories as $key => $category)
            {
                if($key == $active)
                {
                    $string .= "<option selected='true' value='".$key."'>" . $category . "</option>";
                }
                else
                {
                    $string .= "<option value='".$key."'>" . $category . "</option>";
                }
            }
            
            $string .= "</select>";
            
            return $string;
        }
        
        public function display()
        {
            echo "<table>";
            
            echo "
            <tr id='' class='product_header'>
		        <td><!-- --></td>
		        <td>
			        Nom
		        </td>
		        <td>
			        Description
		        </td>
		        <td>
			        Prix
		        </td>
		        <td><!-- --></td>
		        <td><!-- --></td>
	        </tr>";
            
            foreach($this->articles as $article)
            {
            	echo "<tr class='product'>";
				echo "<td><a href='article_view.php?p=-1&id=" . $article->get_id() . "'>
						<img src='products/" . $article->get_img() . "' /></a></td>";
				echo "<td><a href='article_view.php?p=-1&id=" . $article->get_id() . "'>"
                    		. $article->get_name() . "</a></td>";
                echo "<td>" . $article->get_description() . "</td>";
				echo "<td>" . $article->get_price() . "€</td>";
				echo "<td><input id='article_qty_" . $article->get_id() . "' class='qty' type='number' value='1' /></td>";
				echo "<td><input type='button' value='Ajouter' onClick='add_article(" . $article->get_id() . ");' /></td>";
				echo "</tr>";
            }
            
            echo "</table>";
        }
		
		public function display_admin()
		{
			echo "<table>";
			echo "<tr class='product_header'>";
			echo "<td>Modifier la photo</td>";
			echo "<td></td>";
			echo "<td>Nom</td>";
			echo "<td>Description</td>";
			echo "<td colspan='2'>Prix</td>";
			echo "<td>Quantité en stock</td>";
			echo "<td>État de production</td>";
			echo "<td></td>";
			echo "<td></td>";
			echo "</tr>";
			
			
            foreach($this->articles as $article)
            {
				echo "<form method='POST' action='actions/upload_picture.php?pic=" . $article->get_id() . "' enctype='multipart/form-data'>";
            	echo "<tr class='product'>";
            	echo "<td>";
				echo "<input type='file' name='file_name'><br/>";
				echo "<input type='submit' value='Envoyer'></td>";
				echo "</form>";
				echo "<form method='POST' action='actions/mod_article.php'>";
				echo "<td><a href='article_view.php?p=-1&id=" . $article->get_id() . "'>
						<img src='products/" . $article->get_img() . "' /></a></td>";
				echo "<td><textarea cols='10' name='name'>". $article->get_name() ."</textarea></td>";
				echo "<td><textarea name='description'>". $article->get_description() ."</textarea></td>";
				echo "<td><input class='tiny_input' type='text' name='price' value='" . $article->get_price() . "'</td>";
				echo "<td>€</td>";
				echo "<td><input class='tiny_input' type='number' name='stock' value='". $article->get_stock() ."' /></td>";
				echo "<td>
						<select name='state'>
							<option value='" . $article->get_state() . "'>" . $article->get_state() . "</option>
							<option value='nouveaute'>nouveaute</option>
							<option value='disponible'>disponible</option>
							<option value='hors stock'>hors stock</option>
						</select>" . $this->display_categories($article->get_category()) . "
					</td>";
				echo "<input type='hidden' name='id' value='". $article->get_id()."'/>";
				echo "<td><input type='submit' value='Modifier' /></td>";
				echo "<td><a href='actions/del_article.php?del=". $article->get_id() ."'><img class='remove' title='Supprimer cet article' src='images/remove.png' /></a></td>";
				echo "</form>";
				echo "</tr>";
            }
			echo "<tr>";
			echo "<td colspan='2' class='no_tiny' >Nouveau produit :</td>";
			echo "<form method='POST' action='actions/add_article.php'>";
			echo "<td><textarea cols='10' name='name'></textarea></td>";
			echo "<td><textarea name='description'></textarea></td>";
			echo "<td><input class='tiny_input' type='text' name='price' /></td>";
			echo "<td>€</td>";
			echo "<td><input class='tiny_input' type='number' name='stock' /></td>";
			echo "<td>
					<select name='state'>
						<option value='nouveaute'>nouveaute</option>
						<option value='disponible'>disponible</option>
						<option value='hors stock'>hors stock</option>
					</select>" . $this->display_categories(0) . "
				</td>";
			echo "<td><input type='submit' value='Ajouter' /></td>";
			echo "<td></td>";
			echo "</tr>";
            echo "</table>";
		}
    }
?>
