Vue détaillée d'un article
--------------------------------------------------------------------------------

Ressources intervenantes :
    - Jérôme
    - Antoine-Ali

Tâches :
    - Organisation visuelle
    - Informations de l'article
    - Ajout au panier

Avancement : 0%

Date de livraison prévue : 21/06/12

Version : 0.1

Remarques :

    ...

Date de rendu : __/__/__

Signature des développeurs :

Signature du chef de projet :

Signature du responsable :

