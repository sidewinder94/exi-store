<?php
	include_once("classes/article.class.php");
	include_once("classes/basket.class.php");
	include_once("classes/catalog.class.php");
	include_once("classes/user.class.php");

	session_start();
?>
<!doctype html>
<html>

	<head>
		<title>eXi@store - Musique, film, DVD, Jeux vidéo et bien plus...</title>
		<meta charset="utf8" />
		<link rel="stylesheet" type="text/css" href="style.css" />
		<link rel="icon" type="image/png" media="screen" href="favicon.png" />
		<link rel="shortcut icon" type="image/x-ico" media="screen" href="favicon.ico" />
		<link href="css/webwidget_slideshow_dot.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
		<script type="text/javascript" src="js/webwidget_slideshow_dot.js"></script>
	</head>
	
	<body>
		<script type="text/javascript">
            $(function() {
                $("#webwidget_slideshow_dot").webwidget_slideshow_dot({
                    slideshow_time_interval: '5000',
                    slideshow_window_width: '698',
                    slideshow_window_height: '349',
                    soldeshow_foreColor: '#bf000a',
                    directory: 'slideshow/'
                });
            });
        </script>
		
		<div id="global">
		
		<header>
			<?php include("./include/header_include.php"); ?>
		</header>
		
		<div id="content">
			<table>
				<tr>
					<?php include("./include/news_include.php"); ?>
					<?php include("./include/basket_include.php");?>
				</tr>
				
				<tr>
				    <?php include_once("./classes/create_catalog.php"); /* do not use this otherwhere ! */ ?>
					<?php include("./include/search_include.php"); ?>
				</tr>
				
				<tr>
					<?php include("./include/catalog_include.php"); ?>
				</tr>
			</table>
		</div>
		
		<footer>
			<?php include("./include/footer_include.php"); ?>
		</footer>
		
		</div>
		
	</body>

</html>
