<?php

	class Article
    {
        private $id;
        private $name;
        private $description;
        private $price;
		private $state;
		private $stock;
		private $category;
    
        public function __construct($id, $name, $description, $price, $state, $stock, $category)
        {
            $this->id = $id;
            $this->name = $name;
            $this->description = $description;
			$this->price = $price;
			$this->state = $state;
			$this->stock = $stock;
			$this->category = $category;
        }
        
        public function set_id($id)
        {
            $this->id = $id;
        }
        
        public function get_id()
        {
            return $this->id;
        }
        
        public function set_name($name)
        {
            $this->name = $name;
        }
        
        public function get_name()
        {
            return $this->name;
        }
        
        public function get_description()
        {
            return $this->description;
        }
        
        public function get_img()
        {
            return $this->id . ".gif";
        }
        
        public function set_price($price)
        {
            $this->price = $price;
        }
        
        public function get_price()
        {
            return $this->price;
        }
        
        public function set_state($state)
        {
            $this->state = $state;
        }
        
        public function get_state()
        {
            return $this->state;
        }
        
        public function set_stock($stock)
        {
            $this->stock = $stock;
        }
        
        public function get_stock()
        {
            return $this->stock;
        }
        
        public function set_category($category)
        {
            $this->category = $category;
        }
        
        public function get_category()
        {
            return $this->category;
        }
    }

?>
