<?php

    /* the SelectedArticle class
       extends Article, adding the quantity attribute
    */
	class SelectedArticle extends Article
    {
        private $qty;
        
        public function set_qty($qty)
        {
            $this->qty = $qty;
        }
        
        public function get_qty()
        {
            return $this->qty;
        }
        
        /* add a number to the quantity without needing to
           know it */
        public function add_qty($qty)
        {
            $this->qty += $qty;
        }
    }
    
    /* the basket class has add, delete and display methods */
    class Basket
    {
        private $articles = array();
        
        /* this method add an article to the basket */
        public function add($article, $qty)
        {
            /* check if article already exists */
            $id = $article->get_id();
            if(isset($this->articles[$id]))
            {
                $this->articles[$id]->add_qty($qty);
            }
            else
            {
                /* create a new selected article */
                $selected = new SelectedArticle($article->get_id(),
                                                $article->get_name(),
                                                $article->get_description(),
                                                $article->get_price(),
                                                $article->get_state());
                $selected->set_qty($qty);
                
                $this->articles[$id] = $selected;
            }
        }
        
        /* delete an article from the basket
           if the article has a quantity inferior to 2, it is immediately deleted,
           otherwise his quantity is decreased by 1 */
        public function del($id)
        {
            if(isset($this->articles[$id]))
            {
                $article = $this->articles[$id];
                
                if($article->get_qty() > 1)
                {
                    $article->add_qty(-1);
                }
                else
                {
                    unset($this->articles[$id]);
                }
            }
        }
        
        /* empty the basket */
        public function del_all()
        {
            $this->articles = array();
        }
        
        /* calculate the total price */
        public function get_total_price()
        {
        	$total = 0.0;
        	
        	if(!empty($this->articles))
            {
            	
            	foreach($this->articles as $article)
                {
                	$total += $article->get_price() * (float) $article->get_qty();
                }
            }
            
            return $total;
        }
        
        /* return all the articles (useful for loops) */
        public function get_articles()
        {
            return $this->articles;
        }
        
        /* output the HTML from the data */
        public function display()
        {
        	?>
        	<table>
				<tr class="image">
					<td colspan="3"><img src="images/basket.png" /></td>
				</tr>
			<?php
            
            if(!empty($this->articles))
            {
                foreach($this->articles as $article)
                {
                    echo "<tr class='product'>";
                    
                    echo "<td><a href='javascript: del_article(" . $article->get_id() . ");'><img src='images/remove.png' /></a></td>";
                    echo "<td><a href='article_view.php?p=-1&id=" . $article->get_id() . "'>"
                    		. $article->get_name() . "</a></td>";
                    echo "<td class='qty'>" . $article->get_qty() . "</td>";
                    
                    echo "</tr>";
                }
                
                ?>
                <tr class="erase">
					<td colspan="3"><a href="#" onClick="del_all_article();">Vider le panier</a></td>
				</tr>
				<tr class="product">
					<td colspan="2">Total : </td>
					<td class="qty"><?php echo $this->get_total_price(); ?>€</td>
				</tr>
				<tr class="validate">
					<td colspan="3"><input type="button" value="Commander !" onClick="order();" /></td>
				</tr>
				<?php
            }
            else
            {
            	?>
                <tr class="product">
					<td colspan="3">Le panier est vide</td>
				</tr>
				<?php
            }
            
            ?> </table> <?php
        }
        
        /* this output is used to display the basket in the basket page, where
           it takes a bigger place */
        public function display_like_catalog()
        {
        	?>
        	<table>
				<tr class="image">
					<td colspan="6"><img src="images/basket.png" /></td>
				</tr>
			<?php
            
            if(!empty($this->articles))
            {
            	?>
            	<tr class="product_header">
            		<td></td>
					<td>Nom du produit</td>
					<td>Description</td>
					<td>Qté</td>
					<td>Prix</td>
					<td>Prix total</td>
				</tr>
				<?php
            	
                foreach($this->articles as $article)
                {
                    echo "<tr class='product'>";
                    
                    echo "<td><a href='article_view.php?p=-1&id=" . $article->get_id() . "'>
						<img class='img' src='products/" . $article->get_img() . "' /></a></td>";
                    echo "<td><a href='article_view.php?p=-1&id=" . $article->get_id() . "'>"
                    		. $article->get_name() . "</a></td>";
                    echo "<td>" . $article->get_description() . "</td>";
                    echo "<td class='qty'>" . $article->get_qty() . "</td>";
                    echo "<td class='qty'>" . $article->get_price() . "€</td>";
                    echo "<td class='qty'>" . $article->get_qty() * $article->get_price() . "€</td>";
                    
                    echo "</tr>";
                }
                
                ?>
                <tr class="erase">
					<td colspan="6"><a href="#" onClick="del_all_article();">Vider le panier</a></td>
				</tr>
				<tr class="product">
					<td colspan="5">Total : </td>
					<td class="qty"><?php echo $this->get_total_price(); ?>€</td>
				</tr>
				<tr class="validate">
					<td colspan="6">
					    <select id="combobox_card">
					        <option>VISA</option>
					        <option>Mastercard</option>
				        </select>
				        <input type="text" id="credit_card_number" placeholder="Numéro de carte bleue (10 chiffres)" />
				        <input style="width: 50px;" id="security_card_code" type="text" placeholder="XXX"/><br/>
				        <input type="text" id ="expiration_date_card" placeholder="Date d'expiration" />
			        </td>
				</tr>
				<tr class="validate">
					<td colspan="6"><input type="button" value="Commander !" onClick="order();" /></td>
				</tr>
				<?php
            }
            else
            {
            	?>
                <tr class="product">
					<td colspan="3">Le panier est vide</td>
				</tr>
				<?php
            }
            
            ?> </table> <?php
        }
    }
?>

